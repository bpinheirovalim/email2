// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';
import Twilio from 'twilio';

export default (options = {}): Hook => {
    return async (context: HookContext) => {
    const { result } = context;
    
    // console.log( "Number: " + result['to']);

    // start sending message
    const twilioNumber  = process.env.TWILIO_NUMBER; // twilio number
    const accountSid    = process.env.TWILIO_SID;    // twilio SID
    const authToken     = process.env.TWILIO_TOKEN;  // twilio token
    
    const client = require('twilio')(accountSid, authToken);    
    
    function sendText() {
        client.messages.create({
            body: 'Test SMS message',
            from: process.env.TWILIO_NUMBER,
            to: result['to']
        })
        // console.log("Message sent!");
    }
    
    sendText(); 

    return context;
  };
}