import { Application } from '../declarations';
import email from './email/email.service';
import sendSms from './send-sms/send-sms.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application) {
  app.configure(email);
  app.configure(sendSms);
}
